import './App.css';
import Body from './components/Body/Body';
import Header from './components/Header/Header';

function App() {
  return (
    <div className="app">
      <Header></Header>
      <Body></Body>     
    </div>
  );
}

export default App;
