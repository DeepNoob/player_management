import React, { useEffect, useState } from 'react';
import Players from '../Players/Players';
import Team from '../Team/Team';
import './Body.css';
import players from '../../fakeData/playersData.json';

const Body = () => {
    const [player, setPlayer] = useState([]);
    useEffect(() => {
        setPlayer(players)
    },[])

    const [plr, setPlr] = useState([]);

    const addPlayer = (player) =>{
        if(plr.length < 11){
            const newPlr = [...plr, player];
            setPlr(newPlr);            
        }           
            
    }
         
    
    
    return (
        <div className="bodyArea">
            <div className="playersArea">   
            {
                player.map(pl => <Players pl={pl} addPlayer={addPlayer}></Players>)
                
            }   
            </div>
            <div className="teamArea">
                <div>
                    <Team className="teamArea" plr={plr}></Team>
                </div>
                
            </div>
        </div>
    );
};

export default Body;