import React from 'react';
import cover from '../../coverPhoto.png';
import './Header.css';

const Header = () => {
    return (
        <div className="coverImage">
            <img src={cover} alt=""/>                 
        </div>
    );
};

export default Header;