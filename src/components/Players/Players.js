import React from 'react';
import './Players.css';
import Button from 'react-bootstrap/Button';
// import 'bootstrap/dist/css/bootstrap.min.css';

const Players = (props) => {
    const {name, img, age, playingRole, country, price} = props.pl
    const addPlayer = props.addPlayer;
    return (
        <div className="playersArea">
            <div>
                <img src={img} alt=""/>
                <h4 id="namee">{name}</h4>
                <p>Country: {country}</p><p>Age: {age}</p>
                <p>Role: {playingRole}</p>
                <p>Price: {price}</p>
                <Button className="btn" onClick={() => addPlayer(props.pl)}>Hire</Button>
                <hr/>
            </div>
            
        </div>
    );
};

export default Players;