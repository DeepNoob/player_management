import React from 'react';

const Team = (props) => { 
    const plr = props.plr;  
    // console.log(plr);
    let total = 0;
    for(let i = 0; i < plr.length; i++){
        total = total + plr[i].price;
    }
    return (
        <div>
            <h4>No. of Players: {plr.length} </h4>
            
            <p><b>Selected Players-</b></p>
            <ol>
                {
                    plr.map(pl => <li>{pl.name}- {pl.price}</li>)
                }
            </ol>
            <h4>Total: {total}</h4>
        </div>
    );
};

export default Team;